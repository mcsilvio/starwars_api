# Star Wars Survivability Calculator - API

This is a RESTful API server which assists in calculating the probability that the Millenium Falcon will survive its journey to defeat the Deathstar!



## Requirements.

- Python 3.4+
- Virtualenv
- pip3

## Tech Stack Used

- python3
- Swagger generated RESTful Flask server
- networkx - python graph utils library

## Installation & Usage

Perform the following steps to install and use the program.  

1. Clone the repository to a local folder
2. From that folder, set up a virtual python environment by running   
`virtualenv -p python3 env && source env/bin/activate`
3. Install the required dependencies by running  
`pip3 install -r requirements.txt`
4. (Optional) Put in place required Millenium Falcon files (See Usage)
5. (Optional) Configure the port of the server in `starwarsapi/config/configuration.py`
6. Start the server by running  
`python3 -m starwarsapi`

## API documentation

To see detailed documentation on the API, go to `http://localhost:<port>/ui/` in your browser once the server is started.

The swagger specification can be viewed directly in the file  
[swagger-spec.yml](swagger-spec.yml)

## Technical Description

This server provides two main routes: one for supplying only an `empire` file, and one for supplying both an `empire` file and a `millenium falcon` file. For the former, a default `millenium falcon` file will be used. For each of these routes, the given information is used and a probability of survival for the Millenium Falcon is returned. See the API Documentation for more details.

Millenium Falcon files contain a path to a 'routes' database file. That path can simply be a relative path (from the same location as the containing file) or absolute.  There is an important folder for storing these millenium falcon files and database files. It is located at  
`starwarsapi/millenium_falcon_files`

## Author

Silvio Marco Costantini



