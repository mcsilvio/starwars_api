# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "starwarsapi"
VERSION = "1.0.0"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["connexion"]

setup(
    name=NAME,
    version=VERSION,
    description="Star Wars",
    author_email="",
    url="",
    keywords=["Swagger", "Star Wars"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['swagger/swagger.yaml']},
    include_package_data=True,
    entry_points={
        'console_scripts': ['starwarsapi=starwarsapi.__main__:main']},
    long_description="""\
    This is an API which assists in calculating the probability that the Millenium Falcon will survive! Please visit &#x60;http://&lt;host&gt;:8080/ui/&#x60; for documentation.
    """
)
