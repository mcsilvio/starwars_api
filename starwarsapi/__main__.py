#!/usr/bin/env python3

import connexion

from starwarsapi import encoder
from starwarsapi.config.configuration import *

def set_cors_headers_on_response(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'X-Requested-With,origin, X-Auth-Token,Content-Type, accept'
    response.headers['Access-Control-Allow-Methods'] = 'OPTIONS, HEAD, GET, POST, PUT, DELETE'
    return response

def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Star Wars'}, pythonic_params=True)
    app.app.after_request(set_cors_headers_on_response)
    app.run(port=PORT)


if __name__ == '__main__':
    main()
