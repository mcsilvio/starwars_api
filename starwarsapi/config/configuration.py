import os
import connexion
app = connexion.App('starwarsapi')

PORT = 8080

DEFAULT_MILLENIUM_FALCON_FILES_FOLDER = os.path.join(app.root_path, 'millenium_falcon_files')
DEFAULT_MILLENIUM_FALCON_FILE = os.path.join(DEFAULT_MILLENIUM_FALCON_FILES_FOLDER, 'default-millenium-falcon.json')
