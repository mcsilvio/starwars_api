import connexion
import six
import json
from flask import abort

from starwarsapi.config.configuration import *
from starwarsapi.models.empire_activity import EmpireActivity # noqa: E501
from starwarsapi.models.millenium_falcon import MilleniumFalcon # noqa: E501
from starwarsapi.models.empire_activity_and_millenium_falcon import EmpireActivityAndMilleniumFalcon  # noqa: E501
from starwarsapi.models.survivability import Survivability  # noqa: E501
from starwarsapi import util

from starwarsapi.services.probability_calculator import ProbabilityCalculator

def empire_model_is_valid(empire):
    if empire.countdown is None or empire.bounty_hunters is None:
        return False
    return True

def millenium_falcon_model_is_valid(falcon):
    if not falcon.autonomy or not falcon.departure or not falcon.arrival or not falcon.routes_db:
        return False
    return True

def empire_and_mf_model_is_valid(empire_and_mf):
    empire = empire_and_mf.empire_activity
    if not empire or not empire_model_is_valid(empire):
        return False

    millenium_falcon = empire_and_mf.millenium_falcon
    if not millenium_falcon or not millenium_falcon_model_is_valid(millenium_falcon):
        return False
    return True

def survivability_default_config_post(body=None):  # noqa: E501
    """The probability of survival using default config.

    This endoint will return a probability that the Milleniam Falcon will survive. It uses the default configuration, and the given actions of the Empire to perform this calculation. # noqa: E501

    :param body: An object that describes the activity of the Empire&#x27;s bounty hunters.
    :type body: dict | bytes

    :rtype: Survivability
    """
    if connexion.request.is_json:
        empire = EmpireActivity.from_dict(connexion.request.get_json())  # noqa: E501
    
    if not empire_model_is_valid(empire):
        abort(400, "That is not a valid EmpireActivity model")

    # use default millenium falcon file
    with open(DEFAULT_MILLENIUM_FALCON_FILE) as mf_file:
        mf_data = json.load(mf_file)

    millenium_falcon = MilleniumFalcon.from_dict(mf_data)

    probability_calculator = ProbabilityCalculator(empire, millenium_falcon)
    probability_of_survival = probability_calculator.get_probability_of_survival()
    return Survivability(probability_of_survival)


def survivability_post(body=None):  # noqa: E501
    """The probability of survival using a custom config.

    This endoint will return a probability that the Milleniam Falcon will survive. It uses the given configuration, and the given actions of the Empire to perform this calculation. # noqa: E501

    :param body: An object that describes the activity of the Empire&#x27;s bounty hunters.
    :type body: dict | bytes

    :rtype: Survivability
    """
    if connexion.request.is_json:
        empire_and_mf = EmpireActivityAndMilleniumFalcon.from_dict(connexion.request.get_json())  # noqa: E501

    if not empire_and_mf_model_is_valid(empire_and_mf):
        abort(400, "That is not a valid EmpireActivity model")

    empire = empire_and_mf.empire_activity
    millenium_falcon = empire_and_mf.millenium_falcon

    probability_calculator = ProbabilityCalculator(empire, millenium_falcon)
    probability_of_survival = probability_calculator.get_probability_of_survival()
    return Survivability(probability_of_survival)
