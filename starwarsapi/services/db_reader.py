import sqlite3
from sqlite3 import Error
 
class SqliteReader: 

    def __init__(self, db_file): 
        self.db_file = db_file

    def __create_connection(self ):
        conn = None
        try:
            conn = sqlite3.connect(self.db_file)
        except Error as e:
            print(e)
        return conn
     
     
    def __perform_select_all(self, conn):
        cur = conn.cursor()
        cur.execute("SELECT * FROM routes")
        return cur.fetchall()
     
    def select_all_routes(self):
        conn = self.__create_connection()
        with conn:
            return self.__perform_select_all(conn)
        conn.close()

