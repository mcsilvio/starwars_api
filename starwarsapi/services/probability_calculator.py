import os
from starwarsapi.config.configuration import *
from starwarsapi.services.db_reader import SqliteReader
import networkx as nx
from decimal import Decimal, getcontext
getcontext().prec = 2

class ProbabilityCalculator: 

    def __init__(self, empire_activity, millenium_falcon): 
        #inputs
        self.empire_activity = empire_activity
        self.millenium_falcon = millenium_falcon

        #data structures
        self.routes = {}
        self.names_to_ints = {}
        self.ints_to_names = {}

    def __get_days_to_planet(self, source_int, destination_int):
        return self.routes[source_int][destination_int]
    
    def __is_bounty_hunter_present(self, day, planet_int):
        bounty_hunters = self.empire_activity.bounty_hunters
        list_of_bounty_hunters_present = list(
            filter(lambda bh: bh["day"] is day and bh["planet"] is planet_int, \
            map(lambda bh: { "day": bh.day, "planet": self.names_to_ints[bh.planet] }, bounty_hunters)))
        return len(list_of_bounty_hunters_present) > 0
    
    def __refuel(self, current_fuel, current_day):
        return (self.millenium_falcon.autonomy, (current_day + 1))
    
    def __calculate_survival_percentage_from_num_bounty_hunter_hits(self, num_bounty_hunter_hits):
        prob_of_failure = Decimal(0)
        for i in range(0, num_bounty_hunter_hits):
            prob = Decimal(9**i) / Decimal(10**(i+1))
            prob_of_failure += prob
        prob_of_survival = (1 - prob_of_failure) * 100
        return prob_of_survival

    def __get_absolute_path_of_db_file_specified(self, db_file_specified):
        if db_file_specified.startswith("/"):
            return db_file_specified
        else:
            return os.path.join(DEFAULT_MILLENIUM_FALCON_FILES_FOLDER, db_file_specified)

    #this method, performs a refuel, and then continues the simulation to see if path is not disqualified
    def __can_wait_one_day(self, path_portion, current_planet, current_fuel, current_day, autonomy, countdown):
        clone_path = path_portion[:]
        clone_path.insert(0, current_planet)

        days_remaining = countdown - current_day
        current_planet = clone_path.pop(0)
        next_planet = clone_path[0]

        #perform refuel
        days_remaining -= 1
        current_fuel = autonomy
        while True:

            days_to_next_planet = self.__get_days_to_planet(current_planet, next_planet)
            full_fuel_is_enough =  autonomy >= days_to_next_planet
            if not full_fuel_is_enough:
                return False #disqualify
            
            enough_time = days_remaining >= days_to_next_planet
            if not enough_time:
                return False #disqualify
              
            enough_fuel = current_fuel >= days_to_next_planet
            if not enough_fuel:
                #refuel
                days_remaining -= 1
                current_fuel = autonomy
                continue
                
            # go to next planet
            finished = len(clone_path) < 2
            if finished:
                break
            else:
                current_planet = clone_path.pop(0)
                next_planet = clone_path[0]
                current_fuel -= days_to_next_planet
                days_remaining -= days_to_next_planet
        
        #not disqualified, so can wait
        return True 

    def get_probability_of_survival(self):
        db_file_specified = self.millenium_falcon.routes_db
        db_file_absolute = self.__get_absolute_path_of_db_file_specified(db_file_specified)
        if not os.path.isfile(db_file_absolute):
            raise Exception("Routes file not found. Given path (in absolute form): " + db_file_absolute) 

        routes_rows = SqliteReader(db_file_absolute).select_all_routes()

        #create bidirectional graph
        gr = nx.Graph() 
        
        #create names to int index mapping
        node_counter = 0
        for row in routes_rows:
            source = row[0]
            destination = row[1]
        
            #source
            if source not in self.names_to_ints:
                #generate int id
                self.names_to_ints[source] = node_counter
                source_int = node_counter
                node_counter += 1
              
                #add node to graph
                gr.add_node(source_int)
            else:
                source_int = self.names_to_ints[source]
        
            #destination
            if destination not in self.names_to_ints:
                #generate int id
                self.names_to_ints[destination] = node_counter
                destination_int = node_counter
                node_counter += 1
              
                #add node to graph
                gr.add_node(destination_int)
            else:
                destination_int = self.names_to_ints[destination]

            #add node and edge
            gr.add_edge(source_int, destination_int)
        
        
        #create reverse node index mapping
        for (i, (key, value)) in enumerate(self.names_to_ints.items()):
            self.ints_to_names[value] = key
        
        #create data structure from rows (using int ids)
        for row in routes_rows:
            source = row[0]
            destination = row[1]
            weight = row[2]
        
            source_int = self.names_to_ints[source]
            destination_int = self.names_to_ints[destination]
           
            #populate routes indexed data structure
            if source_int not in self.routes:
                self.routes[source_int] = {}
            self.routes[source_int][destination_int] = weight
            
            #and the reverse mapping
            if destination_int not in self.routes:
                self.routes[destination_int] = {}
            self.routes[destination_int][source_int] = weight
        
        source_int = self.names_to_ints[self.millenium_falcon.departure]
        destination_int = self.names_to_ints[self.millenium_falcon.arrival]
        
        paths = nx.all_simple_paths(gr, source=source_int, target=destination_int) 
        
        #print("DEBUG")
        #print(self.names_to_ints)
        #print(self.ints_to_names)
        #print(self.routes)
        #print(paths)
        
        valid_path_probs = [ 0 ]
        autonomy = self.millenium_falcon.autonomy
        countdown = self.empire_activity.countdown
                
        for path in paths:
            clone_path = path[:]
            current_day = 0
            current_fuel = autonomy
            disqualified = False
            num_bounty_hunter_hits = 0
            current_planet = clone_path.pop(0)
            next_planet = clone_path[0]
        
            while True:

                bounty_hunter_is_here_now = self.__is_bounty_hunter_present(current_day, current_planet)
                if bounty_hunter_is_here_now:
                    num_bounty_hunter_hits += 1
        
                days_to_next_planet = self.__get_days_to_planet(current_planet, next_planet)
                date_after_next_leg = current_day + days_to_next_planet
                days_remaining = countdown - current_day
                full_tank_enough = days_to_next_planet <= autonomy
                enough_time = date_after_next_leg <= countdown
                if not full_tank_enough or not enough_time:
                    disqualified = True
                    break
                    
                enough_fuel = days_to_next_planet <= current_fuel
                if not enough_fuel:
                    current_fuel, current_day = self.__refuel(current_fuel, current_day)
                    continue

                #calculate if should wait on this planet. expensive, so only do if necessary
                should_wait = False
                enemy_will_be_at_next_planet = self.__is_bounty_hunter_present(date_after_next_leg, next_planet)
                if enemy_will_be_at_next_planet:
                    can_wait_here = self.__can_wait_one_day(clone_path, current_planet, current_fuel, current_day, autonomy, countdown)
                    if can_wait_here:
                        should_wait = True
                
                if should_wait:
                    current_fuel, current_day = self.__refuel(current_fuel, current_day)
                    continue
                    
                # go to next planet
                finished = len(clone_path) < 2
                if finished:
                    break
                else:
                    current_planet = clone_path.pop(0)
                    next_planet = clone_path[0]
                    current_fuel -= days_to_next_planet
                    current_day += days_to_next_planet
                
            if not disqualified:
                path_prob_of_survival = self.__calculate_survival_percentage_from_num_bounty_hunter_hits(num_bounty_hunter_hits)
                valid_path_probs.append(path_prob_of_survival)
        
        print(max(valid_path_probs))
        return max(valid_path_probs)
            
            
            
