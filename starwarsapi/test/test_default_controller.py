# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from starwarsapi.models.empire_activity import EmpireActivity  # noqa: E501
from starwarsapi.models.empire_activity_and_millenium_falcon import EmpireActivityAndMilleniumFalcon  # noqa: E501
from starwarsapi.models.survivability import Survivability  # noqa: E501
from starwarsapi.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_survivability_default_config_post(self):
        """Test case for survivability_default_config_post

        The probability of survival using default config.
        """
        body = EmpireActivity()
        response = self.client.open(
            '/survivability/default_config',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_survivability_post(self):
        """Test case for survivability_post

        The probability of survival using a custom config.
        """
        body = EmpireActivityAndMilleniumFalcon()
        response = self.client.open(
            '/survivability',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
